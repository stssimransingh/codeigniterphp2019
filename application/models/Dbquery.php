<?php 
    class Dbquery extends CI_Model{
        function getusers(){
            $data = $this->db->get('users');
            return $data->result_array();
        }
        function getsingleuser($id){
            $this->db->where('id',$id);
            $data = $this->db->get('users');
            return $data->result_array();
        }
        function deleteuser($id){
            $this->db->where('id',$id);
            return $this->db->delete('users');
        }
    }
?>