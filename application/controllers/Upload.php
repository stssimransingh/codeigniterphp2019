<?php 
class Upload extends CI_Controller{
    function index(){
        $error['error'] = '';
        $this->load->view('uploadfile',$error);
    }
    function do_upload(){
        $config['upload_path'] = './assets/upload';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload('uploadfile')){
            $error['error'] =$this->upload->display_errors();
            $this->load->view('uploadfile',$error);
        }else{
            echo "Success";
            $data = $this->upload->data();
            $filename = $data['file_name'];
            $url['imgurl'] = base_url('assets/upload/'.$filename);
            $this->load->view('success',$url);
            
        }
    }
}
?>