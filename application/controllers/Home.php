<?php 
// base_path :/ index.php/controller/method
// http://localhost/code/index.php/home/index

class Home extends CI_Controller{
    public function index(){
        $data['pagename'] = "Home Page";
        $this->load->view('homepage',$data);
    }
    public function adddata(){
        $data = $this->input->post();
        
        if($this->db->insert('users',$data)){
            echo "Data Inserted";
            redirect('home');
        }else{
            echo "Error";
        }

    }
    public function about(){
       echo "Success";
    }
    function show_data(){
        $this->load->model('dbquery');
        $data['users'] = $this->dbquery->getusers();
        $this->load->view('showdata',$data);
    }
    function edit_user(){
        if($this->input->get('uid')){
            $id = $this->input->get('uid');
            $this->load->model('dbquery');
            $data['userdata'] = $this->dbquery->getsingleuser($id);
            $this->load->view('edituser',$data);
        }else{
            echo "Sorry User not found";
        }
    }
    function edit_user_data(){
        if($this->input->get('uid')){
            $id = $this->input->get('uid');
            $data = $this->input->post();
            $this->db->where('id',$id);
            if($this->db->update('users',$data)){
                redirect('home/show_data');
            }else{
                echo "Error";
            }
        }else{
            echo "Please send uid as well.";
        }
    }
    function deleteuser(){
        if($this->input->get('uid')){
            $id = $this->input->get('uid');
            $this->load->model('dbquery');
            if($this->dbquery->deleteuser($id)){
                echo "Deleted";
                redirect('home/show_data');
            }else{
                echo "Error";
            }
        }else{
            echo "Not Allowed";
        }
    }
}
?>