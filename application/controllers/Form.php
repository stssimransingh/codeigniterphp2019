<?php 
    class Form extends CI_Controller{
        function index(){
            $this->load->view('myform');
        }
        function success(){
            echo "<h1>Success</h1>";
        }
        function getdata(){
            $this->form_validation->set_rules('username', 'Username', 'callback_username_check|required|min_length[5]|max_length[12]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
            $this->form_validation->set_rules('email', 'Email', 'required');
            if ($this->form_validation->run() == FALSE)
                {
                    $this->load->view('myform');
                }
            else
            {
                   redirect('form/success');
            }
        }
        public function username_check($str)
        {   
            $pattren = "/ /";
            $result = preg_match($pattren, $str);
                if ($result)
                {
                        $this->form_validation->set_message('username_check', 'Space Not allowed in username');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }
    }
?>