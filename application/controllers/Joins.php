<?php
class Joins extends CI_Controller{
    function index(){

    }
    function addcollege(){
        if($this->input->post('collegename')){
            $data = $this->input->post();
            if($this->db->insert('college',$data)){
                echo "Success";
            }
        }
        $this->load->view('addcollege');
    }
    function adduser(){
        $data['colleges'] = $this->db->get('college')->result_array();
        $this->load->view('adduser',$data);
        if($this->input->post('collegeid')){
            $userdata = $this->input->post();
            $this->db->insert('usersdata',$userdata);
            echo "Success";
        }

    }
    function showuser(){
        $this->db->select('usersdata.*, college.collegename');
        $this->db->from('usersdata');
        $this->db->join('college', 'usersdata.collegeid = college.id');
        $data['users'] = $this->db->get()->result_array();
        $this->load->view('showcollegedata',$data);
    }
}
?>