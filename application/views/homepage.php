<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $pagename; ?></title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">

</head>
<body class="bg-info">
    <form method="post" action="<?php echo base_url('index.php/home/adddata'); ?>">
        <input type="text" name="firstname" placeholder="firstname" />
        <input type="text" name="lastname" placeholder="lastname"/>
        <select name="gender" >
            <option>Male</option>
            <option>Female</option>
        </select>
        <input type="submit">
    </form>
</body>
</html>