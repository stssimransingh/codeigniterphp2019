<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
</head>
<body>
    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <th>Sr.No</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 1;
                foreach ($users as $key => $value) {
                    echo "<tr>";
                    echo "<td>".$i++."</td>";
                    echo "<td>$value[firstname]</td>";
                    echo "<td>$value[lastname]</td>";
                    echo "<td>$value[gender]</td>";
                    echo "<td><a href='".base_url('index.php/home/edit_user?uid='.$value['id'])."' class='btn btn-warning'>Edit</a>
                    <a href='".base_url('index.php/home/deleteuser?uid='.$value['id'])."' class='btn btn-danger'>Delete</a>
                    </td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
</body>
</html>