<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <label for="">College</label>
        <select name="collegeid">
            <?php 
                foreach ($colleges as $college) {
                    echo "<option value='$college[id]'>$college[collegename]</option>";
                }
            ?>
        </select>
        <label for="">Username</label>
        <input type="text" name="username" />
        <label for="">Password</label>
        <input type="password" name="password" />
        <label for="">Full Name</label>
        <input type="text" name="fullname" />
        <input type="submit">
    </form>
</body>
</html>