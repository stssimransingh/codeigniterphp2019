<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form method="post" action="<?php echo base_url('index.php/home/edit_user_data?uid='.$userdata[0]['id']); ?>">
        <label for="">First Name</label>
        <input type="text" name="firstname" value="<?php echo $userdata[0]['firstname']; ?>" /><br/>
        <label for="">Last Name</label>
        <input type="text" name="lastname" value="<?php echo $userdata[0]['lastname']; ?>" /><br/>
        <label for="">Gender</label>
        <?php 
            $male = '';
            $female = '';
            if($userdata[0]['gender']== 'Male'){
                $male = 'selected';
            }else{
                $female = 'selected';
            }
        ?>
        <select name="gender">
            <option <?php echo $male; ?>>Male</option>
            <option <?php echo $female; ?>>Female</option>
        </select>
        <input type="submit" />
    </form>
</body>
</html>