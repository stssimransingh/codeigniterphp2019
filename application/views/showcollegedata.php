<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
</head>
<body>
<!-- <pre>
    <?php 
    // print_r($users);
    ?>
</pre> -->
    <table class="table">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>Name</th>
                <th>College Name</th>
                <th>Username</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach($users as $user){
                echo "<tr>";
                echo "<td>".$i++."</td>";
                echo "<td>$user[fullname]</td>";
                echo "<td>$user[collegename]</td>";
                echo "<td>$user[username]</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>    
</body>
</html>